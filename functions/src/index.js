const { onRequest } = require("firebase-functions/v2/https");
const apiHandlers = require("./handlers/api");

exports.api = onRequest(apiHandlers.callback());
