const db = require("../../database/database");
/**
 *
 * @param ctx
 * @returns {Promise<void>}
 */
async function getItems(ctx) {
    try {
        const res = []
        const itemsRef = db.collection('todoItem');
        const items = await itemsRef.get();
        items.forEach(doc => {
            res.push({
                id: doc.id,
                ...doc.data()
            })
        });
        ctx.body = {
            data: res
        };
    } catch (e) {
        ctx.status = 404;
        ctx.body = {
            success: false,
            data: [],
            error: e.message,
        };
    }
}
async function getItem(ctx) {
    try {
        const { id } = ctx.params;
        const itemRef = db.collection('todoItem').doc(id);
        const item = await itemRef.get();
        if (item.data()) {
            return (ctx.body = {
                data: {
                    id: item.id,
                    ...item.data()
                },
            });
        }

        throw new Error("Item Not Found with that id!");
    } catch (e) {
        ctx.status = 404;
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
async function save(ctx) {
    try {
        const postData = ctx.req.body;
        const data = {
            ...postData,
            status: 2,
        };
        await db.collection("todoItem").add(data);

        ctx.status = 200 || 201;
        return (ctx.body = { message: 'Update successful' })
    } catch (e) {
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
async function update(ctx) {
    try {
        const { id } = ctx.params;
        const postData = ctx.req.body;
        const itemRef = db.collection('todoItem').doc(id);
        const item = await itemRef.get();
        if (item.data()) {
            await itemRef.update({
                status: postData.status
            })
            ctx.status = 200 || 201;
            return (ctx.body = { message: 'Update successful' })
        } else {
            return (ctx.body = {
                success: false,
                error: 'Item not found',
            });
        }
    } catch (e) {
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
async function updateItems(ctx) {
    const postData = ctx.req.body;

    const batch = db.batch();

    postData.ids?.forEach((id) => {
        const docRef = db.collection('todoItem').doc(id);
        batch.update(docRef, { status: postData.status });
    });

    try {
        await batch.commit();
        ctx.status = 200;
        ctx.body = { message: 'Update successful' };
    } catch (e) {
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
async function deleteItem(ctx) {
    try {
        const { id } = ctx.params;
        const itemRef = db.collection('todoItem').doc(id);
        const item = await itemRef.get();
        if (item.data()) {
            await itemRef.delete()
            ctx.status = 200 || 201;
            ctx.body = { message: 'Delete successful' };
        } else {
            return (ctx.body = {
                success: false,
                error: 'Item not found',
            });
        }
    } catch (e) {
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
async function deleteItems(ctx) {
    const postData = ctx.req.body;
    const batch = db.batch();
    postData.forEach((id) => {
        const docRef = db.collection('todoItem').doc(id);
        batch.delete(docRef);
    });
    try {
        await batch.commit();
        ctx.status = 200;
        ctx.body = { message: 'Delete successful' };
    } catch (e) {
        return (ctx.body = {
            success: false,
            error: e.message,
        });
    }
}
module.exports = {
    getItems,
    getItem,
    save,
    update,
    deleteItem,
    deleteItems,
    updateItems
};
