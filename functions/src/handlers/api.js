const Koa = require("koa");
const koaBody = require('koa-bodyparser');
const routes = require("../routes/routes.js");
const cors = require("koa2-cors");

const app = new Koa();

function hybridBodyParser(opts) {
    const bp = koaBody(opts)
    return async (ctx, next) => {
        ctx.request.body = ctx.request.body || ctx.req.body
        return bp(ctx, next)
    }
}

app.use(cors());
app.use(hybridBodyParser())
app.use(routes.routes());
app.use(routes.allowedMethods());

module.exports = app;
