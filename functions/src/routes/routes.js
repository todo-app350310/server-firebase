const Router = require("koa-router");
const bookHandler = require("../handlers/books/bookHandlers");
const itemHandler = require("../handlers/items/itemHandlers");
const bookInputMiddleware = require("../middleware/bookInputMiddleware");
const itemInputMiddleware = require("../middleware/itemInputMiddleware");
// Prefix all routes with /books
const router = new Router({
  prefix: "/api",
});

// Routes will go here

router.get("/books", bookHandler.getBooks);
router.get("/books/:id", bookHandler.getBook);
router.post("/books", bookInputMiddleware, bookHandler.save);

router.get("/items", itemHandler.getItems);
router.get("/items/:id", itemHandler.getItem);
router.post("/items", itemInputMiddleware, itemHandler.save);
router.put("/items/:id", itemHandler.update);
router.put("/items", itemHandler.updateItems);
router.delete("/items/:id", itemHandler.deleteItem);
router.delete("/items", itemHandler.deleteItems);

module.exports = router;
